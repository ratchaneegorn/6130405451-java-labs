package srijanoon.ratchaneegorn.lab3;
import java.util.Scanner;
/**A java program is produces the same result as MatchingPenny. 
 * The program defines three methods, namely acceptInput(),
*genComChoice() and displayWiner() described below. The program also contains two static
*String variable, namely humanChoice and compChoice to be used in the program methods.
* Author: Ratchaneegorn Srijanoon
* ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
 */
class MatchingPennyMethod {
    public static void main(String[] args){
            String inputUser = acceptInput();
            String comChoice = genComChoice();
            String displayWinLose = displayWiner(inputUser ,comChoice);
            System.out.println("You play " + inputUser);
            System.out.println("Computer play " + comChoice);
            System.out.println(displayWinLose);       
    }
    public static String acceptInput(){	
        while(true){
            System.out.print("Enter head or tail: ");
            Scanner inputAns = new Scanner(System.in);
            String inputHeadOrTail = inputAns.nextLine();
            String head = "head";
            String tail = "tail";
            String exit = "exit";
            if(inputHeadOrTail.equalsIgnoreCase(exit)) { 
                System.out.println("Good Bye.");
                System.exit(-1);
                    }
            else if(inputHeadOrTail.equalsIgnoreCase(head)) {
                return "head";
                            }	
            else if (inputHeadOrTail.equalsIgnoreCase(tail)) {
                return "tail";
                            }	
            else{ //for incorrect input.
                System.err.println("Incorrect input.head or tail only");
            }
        }   
    } 
    public static String genComChoice(){
        int random = (int)(Math.random() * ((1- 0) + 1));
        if (random == 1) {
            return "head";
        }
        else{
           return "tail";        
        }
    }
    public static String displayWiner(String inputUser ,String comChoice){
        if (inputUser.equalsIgnoreCase(comChoice)){
            return "You win.";
           
        }
        else{
            return "You lose.";
           
        } 
    }
}
       

 

