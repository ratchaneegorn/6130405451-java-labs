package srijanoon.ratchaneegorn.lab3;
import static java.lang.System.in;
import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;
/**
*The TypingTest program is a java program that test whether a user types faster or slower than an average person. 
* An average person types at the speed of 40 words per minute. The program random 8 colors from a list of rainbow colors 
* (RED,ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET). Color can be picked more than once. 
* A user must type in the same list ignoring case. If the user type in incorrectly, he needs
* to type in again. After the correct input is enter, the time used for typing in will be calculated
* and if he types in faster than or equal to 12 seconds, he types faster than average otherwise
* he types slower than average. The time for typing in can be computed using subroutine currentTimeMillis in class System.
* ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
 */
public class TypingTest {
    public static void main(String[] args){
        String[] color = new String[8];
        String[] colorlist = {"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET"} ;
        while(true){
            for (int i = 0; i < 8; i++) {
                int randomNum = (int)(Math.random() * ((6 - 0) + 1));
                color[i] = colorlist[randomNum];
                System.out.print(colorlist[randomNum] + " ");
            }
            System.out.println("\nType your answer: ");
            Scanner inputAns = new Scanner(System.in);
            Double timestart = (double) System.currentTimeMillis();
            String[] inputAnswer = inputAns.nextLine().toUpperCase().split(" ");
            if(Arrays.equals(color, inputAnswer)){
                Double timestop = (double) System.currentTimeMillis();
                Double time = (timestop - timestart) * 0.001;
                System.out.println("You time is " + time + ".");
                if(time <= 12){
                    System.out.print("You type faster than average person");
                    System.exit(0);
                  }   
                 else {
                    System.out.print("You type slower than average person");
                    System.exit(0);
                break;
                }
            }else{
                continue;
            }  
        } 
    }
}    


	