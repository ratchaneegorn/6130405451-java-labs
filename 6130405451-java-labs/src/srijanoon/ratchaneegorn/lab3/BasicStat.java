package srijanoon.ratchaneegorn.lab3;
import static java.lang.Integer.parseInt;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
*the BasicStst is java program that will find the minimum, the maximum, the
*average and the standard deviation of the list of numbers entered. The first argument is how
*many numbers to be entered. The rest of the program arguments are list of numbers. Thus for
*program argument, the program should check if there are numbers entered as many as the first integer.
* The program includes two methods, namely acceptInput() and displayStats(), and two
*static variables, namely num[] and numInput. The main method of the program should be as
*shown Method acceptInput(), check if the number of entered numbers is the same as
*the first integer entered.If the program arguments are entered correctly, set numInput to
*the first program argument. And the rest of numbers are to be stored with
*double array num[].
* Method displayStats(), show the minimum, the maximum, the average and the
*standard deviation of the numbers entered.
* * ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
 */
public class BasicStat {
	static double[] number;
	static int inputNumber;
        static double deviation = 0;
	static double averrage;
	static void acceptInput(String[] args) {
		inputNumber = Integer.parseInt(args[0]);
		if (inputNumber == (args.length - 1)) {
			number = new double[args.length - 1];
			for (int i = 0; i < args.length - 1; i++) {
			number[i] = Double.parseDouble(args[i + 1]);
			}
		} else {
                    System.err.println("<BasicStat> <numNumbers> <numbers>..."); 
		}
	}
	static double[] minAndMax() { 
		double min = number[0], max = number[0];
		for (int i = 0; i < inputNumber; i++) {
			double checknumber = number[i];
			if (checknumber < min) {
                            min = checknumber;
			}
			if (checknumber > max) {
                            max = checknumber;
			}
		}
		double outputNumber[] = new double[2];
		outputNumber[0] = min;
		outputNumber[1] = max;
		return outputNumber;
	}
	static void findAverage() {
            double total = 0;
            for (int i = 0; i < inputNumber; i++) {
                total += number[i];
            }
		averrage = total / inputNumber;
	}
	static void findStandardDeviation() { 
            for (int i = 0; i < inputNumber; i++) {
                    deviation += Math.pow(number[i] - averrage, 2);
            }
            deviation /= inputNumber;
            deviation = Math.sqrt(deviation);
	}
	static void displayStats() {
            double[] inPutMaxMin = minAndMax();
            findAverage();
            findStandardDeviation();
            System.out.printf("Max is %s  Min is %s\n", inPutMaxMin[1], inPutMaxMin[0]);
            System.out.printf("Average is %s\n", averrage); 
            System.out.printf("Standard Deviation is %s", deviation);
	}
        public static void main(String[] args) {
            acceptInput(args);
            displayStats();
	}
	
}

