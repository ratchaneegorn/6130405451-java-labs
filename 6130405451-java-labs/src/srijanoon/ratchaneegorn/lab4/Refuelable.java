package srijanoon.ratchaneegorn.lab4;
/**
 * Author : Ratchaneegorn srijanoon
 * ID: 613040545-1
 * Sec: 1
 * Date: February 4, 2019
 */

//interface Refuelable has 1 method.
public interface Refuelable {
	public void refuel();
	
}


