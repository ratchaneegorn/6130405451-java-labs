package srijanoon.ratchaneegorn.lab4;
/**
 * Author : Ratchaneegorn srijanoon
 * ID: 613040545-1
 * Sec: 1
 * Date: February 4, 2019
 */

 //interface moveable has 3 method.
public interface Moveable{ 
        public void accelerate();     
        public void brake();
        public void setspeed();
}