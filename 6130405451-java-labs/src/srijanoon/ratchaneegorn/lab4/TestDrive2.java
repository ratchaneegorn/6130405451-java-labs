package srijanoon.ratchaneegorn.lab4;   
/**
*TestDrive2 that is the main method as shown in ToyotaAuto and the output as shown in HondaAuto
*has method isFaster() that compares if the first automobile is faster than the
*second automobile as shown in ToyotaAuto and HondaAuto.
* 
*/
public class TestDrive2 {
    public static void main(String[] args){
        ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
        HondaAuto car2 = new HondaAuto(200, 8, "City");
        
        System.out.println(car1);
        System.out.println(car2);
         
        car1.accelerate();
        car2.accelerate();
        car2.accelerate();
        
        System.out.println(car1);
        System.out.println(car2);
        
        car1.brake();
        car1.brake();
        car2.brake();
        
        System.out.println(car1);
        System.out.println(car2);
        
        car1.rafuel();
        car2.refuel();
        System.out.println(car1);
        System.out.println(car2);
        isFaster(car2, car1);
        isFaster(car2, car1);
        
         
    }

    private static void isFaster(HondaAuto car2, ToyotaAuto car1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
