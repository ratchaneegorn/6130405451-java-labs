package srijanoon.ratchaneegorn.lab4;
import srijanoon.ratchaneegorn.lab4.Automobile;
/**
 * Author : Ratchaneegorn srijanoon
 * ID: 613040545-1
 * Sec: 1
 * Date: February 4, 2019
 */
//extends Automobile implements Moveable, Refuelable
public class ToyotaAuto extends Automobile implements Moveable, Refuelable{ 
    //public constuctor that accepts int gasoline, int speed, int maxSpeed, int acceleration, String model
    public ToyotaAuto(int gasoline, int speed, int maxSpeed, int acceleration, String model) {
        setGasoline(100);
        this.setMaxSpeed(maxSpeed);
	this.setAcceleration(acceleration);;
	this.setModel(model);
    }

    ToyotaAuto(int i, int i0, String vios) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
//Method that sets the value of gasoline to 100 and output message 'refuels'.
    public void refuel() {
        this.setGasoline(100);
        System.out.println(this.getModel() + "refuels");
    }
  /*
   method that increases the current speed by the acceleration.
   check if the new speed does not exceed the maximum speed set the new speed to the maximum speed. 
   The method decreases gasoline by 15 and also displays message “accelerates”
    */
    public void accelerate() {
        if (this.getSpeed() + this.getAcceleration() <= this.getMaxSpeed()) {
            this.setSpeed(this.getSpeed() + this.getAcceleration());
            this.setGasoline(this.getGasoline() - 15);
        }
        else {
                this.setSpeed(this.getMaxSpeed());
                this.setGasoline(this.getGasoline() - 15);
                }
        System.out.println(this.getModel() + "accelerates" );
        }
//The method must check if the new speed is not less than 0 set the new speed to 0.
    public void brake() {
        if (this.getSpeed() - this.getAcceleration() >= 0) {  //check value of gasoline by 15 and also displays message “brakes” .
                this.setSpeed(this.getSpeed() - this.getAcceleration());
                this.setGasoline(this.getGasoline() - 15);
                }
        else {
                this.setSpeed(0);
                this.setGasoline(this.getGasoline() - 15);
                }
        
        System.out.println(this.getModel() + "brakes");
        }
//method is accepts one integer for setting the speed and check if the input speed must neither be negative nor exceed maxSpeed. 
    public void setSpeed(int speed) { 
        if(speed < 0){
            //Otherwise set speed to 0 or maxSpeed accordingly.
            this.setSpeed(0);
        }
        else if (speed > this.getMaxSpeed()) {
            this.setSpeed(this.getMaxSpeed());;
        }	
        else {
            this.setSpeed(speed);
                }
    }

    void rafuel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setspeed() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
