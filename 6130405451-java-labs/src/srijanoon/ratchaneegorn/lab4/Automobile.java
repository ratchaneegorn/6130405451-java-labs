package srijanoon.ratchaneegorn.lab4;
/**
 * Author : Ratchaneegorn srijanoon
 * ID: 613040545-1
 * Sec: 1
 * Date: February 4, 2019
 */
public class Automobile {
    	
	// public methods to set and get each information.	
	public int getGasoline() {
		return gasoline;
	}
	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public int getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public static int getNumberOfAutomobile() {
		return NumberOfAutomobile;
	}
	public static void setNumberOfAutomobile(int numberOfAutomobile) {
		NumberOfAutomobile = numberOfAutomobile;
		
	}
	@Override
	public String toString() {
		return "AutoMobile [gasoline=" + gasoline + ", speed=" + speed + ", maxSpeed=" + maxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}
        //private instance variables to record the information.
        private int gasoline;
	private int speed;
	private int maxSpeed;
	private int acceleration;
	private String model;
	private Color color;
	private static int NumberOfAutomobile = 0;	
	enum Color{
			RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE, BLACK;
		}
        //A constructor which accepts values for the fields gasoline, speed, maxSpeed, acceleration, model, color.
	public Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		NumberOfAutomobile += 1;
	}
        //A constructor is no input, but initializes gasoline, speed, acceleration to 0 and maxSpeed to 160, model to “Automobile”, and color to  WHITE
	public Automobile(){
		gasoline = 0;
		speed = 0;
		acceleration = 0;
		maxSpeed = 160;
		model = "Automobile";
		color = Color.WHITE;
		NumberOfAutomobile += 1;
	}
	

}
