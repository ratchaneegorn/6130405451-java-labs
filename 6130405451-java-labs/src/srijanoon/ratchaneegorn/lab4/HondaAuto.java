package srijanoon.ratchaneegorn.lab4;
/*
 * Author : Ratchaneegorn srijanoon
 * ID: 613040545-1
 * Sec: 1
 * Date: February 4, 2019
 */
//extends Automobile and implements Movable, Refuelable.
public class HondaAuto extends Automobile implements Moveable, Refuelable { 
    //Contractor for making a Automobile with the inputed variable.
    public HondaAuto(int maxSpeed, int acceleration, String model) {
            this.setMaxSpeed(maxSpeed);
            this.setAcceleration(acceleration);
            this.setModel(model);
            setGasoline(100);
    }
    //Method is set Gasoline to 100.
    public void refuel() {
            this.setGasoline(100);
            System.out.printf("%s refules\n", this.getModel());
            }
  /*
    method is increases current speed by the acceleration, but not to exceed the maximum speed. 
    it set the new speed to the maximum speed, decreases the value of gasoline by 10 and also output message “accelerates” 
    */
    public void accelerate() {
            if (this.getSpeed() + this.getAcceleration() <= this.getMaxSpeed()) {
                    this.setSpeed(this.getSpeed() + this.getAcceleration());
                    this.setGasoline(this.getGasoline() - 10);
                    }
            else {
                    this.setSpeed(this.getMaxSpeed());
                    this.setGasoline(this.getGasoline() - 10);
                    }
            System.out.printf("%s accelerates\n", this.getModel());
            }
//The method must check if the new speed is not less than 0 set the new speed to 0.
    public void brake() {
            if (this.getSpeed() - this.getAcceleration() >= 0) { //check value of gasoline by 10 and also displays message “brakes” .
                    this.setSpeed(this.getSpeed() - this.getAcceleration());
                    this.setGasoline(this.getGasoline() - 10);
                    }
            else {
                    this.setSpeed(0);
                    this.setGasoline(this.getGasoline() - 10);
                    }
            System.out.printf("%s brakes\n", this.getModel());
            }
//method is accepts one integer for setting the speed and check if the input speed must neither be negative nor exceed maxSpeed. 
    public void setSpeed(int speed) {
            if (speed < 0) { 
            //Otherwise set speed to 0 or maxSpeed accordingly.
                    this.setSpeed(0);
                    }
            else if (speed > this.getMaxSpeed()) {
                    this.setSpeed(this.getMaxSpeed());;
                    }	
            else {
                    this.setSpeed(speed);
                    }
            }

    @Override
    public void setspeed() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    }

