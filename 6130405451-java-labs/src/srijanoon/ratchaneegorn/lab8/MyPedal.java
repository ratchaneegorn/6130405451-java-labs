package srijanoon.ratchaneegorn.lab8;
import java.awt.geom.Rectangle2D;

/**
 * 
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 25 Mar 19
 */
public class MyPedal extends Rectangle2D.Double {
        //2 protected final static int that are the width and height of a pedal. 
	protected static int pedalWidth = 100;
	protected static int pedalHeight = 10;
        //The constructor that accepts two parameters then call superclass constructor. 
	protected MyPedal(int x,int y) {
		super(x, y, pedalWidth, pedalHeight);
	}
}