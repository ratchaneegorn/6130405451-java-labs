package srijanoon.ratchaneegorn.lab8;
import javax.swing.SwingUtilities;
/** 
 * class called MyFrameV4 which is extended from MyFrameV3. MyFrameV4 has the methods as shown in Figure 7.
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 1 Apr 19
 */
public class MyFrameV4 extends MyFrameV3{
    
    public MyFrameV4(String text) {
        super(text);
    }
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                createAndShowGUI();
            }
        });
    }
    public static void createAndShowGUI(){
        MyFrameV4 msw = new MyFrameV4("My frame V4");
        msw.addComponents();
        msw.setFrameFeatures();
    }
    protected void addComponents(){
        add(new MyCanvasV4());
    }
}
