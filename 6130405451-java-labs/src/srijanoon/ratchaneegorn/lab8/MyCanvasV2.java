package srijanoon.ratchaneegorn.lab8;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;
/** 
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 1 Apr 19
 */
public class MyCanvasV2 extends MyCanvas { 
        //Declare variable ball for MyBall, pedal for MyPedal and brick for MyBrick.
	MyBall ball = new MyBall(WIDTH / 2 - (MyBall.diameter / 2), HEIGHT / 2  - (MyBall.diameter / 2));
	MyPedal pedal = new MyPedal(WIDTH / 2 - (MyPedal.pedalWidth / 2) ,HEIGHT - MyPedal.pedalHeight);
	MyBrick brick = new MyBrick(WIDTH / 2 - (MyBrick.brickWidth / 2) , 0);
	
	public MyCanvasV2() {
		super();
	}
        //Override method painComponent() to draw.
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
                //Draw rectangle the size of the whole panel  with the color black.
		Rectangle2D.Double rect = new Rectangle2D.Double(0, 0, WIDTH, HEIGHT);
                g2d.fill(rect);
                g2d.setColor(Color.WHITE);
                g2d.drawLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
                g2d.drawLine(WIDTH / 2, 0 , WIDTH / 2,HEIGHT);
                g2d.fill(ball); //Ball in the middle of the panel.
                g2d.fill(brick);//Brick class to be top of the panel.
                g2d.fill(pedal);//Pedal at the bottom of the panel.
                
    	
	
	
		
		
	}
}