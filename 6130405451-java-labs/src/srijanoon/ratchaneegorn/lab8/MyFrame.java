package srijanoon.ratchaneegorn.lab8;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
/**
 * Class MyFrame which has the following properties.
 * Has four methods: main, createAndShowGUI, addComponents and setFrameFeature.
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 25 Mar 19
 */
public class MyFrame extends JFrame {
	public MyFrame(String string) {	
		super(string);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				createAndShowGUI();
			}
		});
			}
	protected static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();
		
	}
	private void addComponents() {
		add(new MyCanvas());
	}
	public void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	

}