package srijanoon.ratchaneegorn.lab8;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.*;
/**
 * class MyCanvas which extends from class JPanel
 * should draw a picture as shown in Figure 2.
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 25 Mar 19
 */
public class MyCanvas extends JPanel {
	protected int WIDTH = 800; 
	protected int HEIGHT = 600; 

	public MyCanvas() {
		//Set background
		super();
		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(WIDTH, HEIGHT)); //Set frame
	}
	//In this method draw a picture as shown in Figure 2. 
	public void paintComponent(Graphics g) { 
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		Ellipse2D.Double oval = new Ellipse2D.Double(WIDTH / 2 - 150, HEIGHT / 2 - 150, 300, 300);
		Ellipse2D.Double ovalEyeL = new Ellipse2D.Double(WIDTH / 2 - 50, HEIGHT / 2 -60, 30, 60);
		Ellipse2D.Double ovalEyeR = new Ellipse2D.Double(WIDTH / 2 + 20, HEIGHT / 2 - 60, 30, 60);
		Rectangle2D.Double rectM = new Rectangle2D.Double(WIDTH / 2 - 50, HEIGHT / 2 + 80, 100, 10);
		g2d.setColor(Color.WHITE);
		g2d.draw(oval);
		g2d.fill(ovalEyeL);		
		g2d.fill(ovalEyeR);		
		g2d.fill(rectM);

	}

}