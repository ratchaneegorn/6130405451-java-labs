package srijanoon.ratchaneegorn.lab8;
import javax.swing.SwingUtilities;
/** 
 * class called MyFrameV3 that is extended from MyFrameV2. MyFrameV3 has the methods as shown in Figure 5.
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 1 Apr 19
 */
public class MyFrameV3 extends MyFrameV2{
	public MyFrameV3(String text) {
            super(text);
	}
        public static void main(String[] args) {
            SwingUtilities.invokeLater(new Runnable(){
                public void run(){
                    createAndShowGUI();
                }
            });
        }
        public static void createAndShowGUI(){
            MyFrameV3 msw = new MyFrameV3("My Frame V3");
            msw.addComponents();
            msw.setFrameFeatures();
        }
        protected void addComponents(){
            add(new MyCanvasV3());
        }
	

}