package srijanoon.ratchaneegorn.lab8;
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
/** 
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 1 Apr 19
 */
public class MyCanvasV3 extends MyCanvasV2 {
	public void paintComponent(Graphics g) { 
		super.paintComponent(g);
                g.setColor(Color.BLACK);
                //Draw a rectangle the size of the whole panel with color black.
                g.fillRect(0, 0, WIDTH, HEIGHT);               
		Graphics2D g2d = (Graphics2D) g;
                //Draw 10 bricks across the middle of the panel.
                for(int i = 0 ; i < 10 ;i++){
                    MyBrick brick = new MyBrick(MyBrick.brickWidth*i, HEIGHT / 2 - (MyBrick.brickHeight / 2));
                    g2d.setColor(Color.WHITE);
                    g2d.fill(brick);
                    g2d.setColor(Color.BLACK);
                    g2d.setStroke(new BasicStroke(5));
                    g2d.draw(brick);
                    
                }
                //The four ball must be in array or list of MyBall.
                MyBall ball = new MyBall(WIDTH - (MyBall.diameter), HEIGHT  - (MyBall.diameter));
                g2d.setColor(Color.WHITE);
                MyBall ball1 = new MyBall(0, 0);
                MyBall ball2 = new MyBall(0, HEIGHT - (MyBall.diameter));
                MyBall ball3 = new MyBall(WIDTH - (MyBall.diameter), 0);
                g2d.fill(ball);
                g2d.fill(ball1);
                g2d.fill(ball2);
                g2d.fill(ball3);
                g2d.setColor(Color.BLACK);
                g2d.draw(ball);
                g2d.draw(ball1);
                g2d.draw(ball2);
                g2d.draw(ball3);     
	}
}