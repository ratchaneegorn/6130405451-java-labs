package srijanoon.ratchaneegorn.lab8;
import java.awt.*;
/** 
 * Create a class called MyCanvasV4 which is extended from MyCanvasV3 has the following properties.
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 1 Apr 19
 */
public class MyCanvasV4 extends MyCanvasV3{
    protected Color[] color = {Color.MAGENTA,Color.BLUE,Color.CYAN,Color.GREEN,Color.YELLOW,Color.ORANGE,Color.RED};
    public void paintComponent(Graphics g) { 
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        Graphics2D g2d = (Graphics2D) g;
        //Draw bricks with 7 rows each rows with 10 bricks.the bricks has a different color.
        for(int j = 0; j < 7;j++){
            for(int i = 0 ; i < 10 ;i++){
                        MyBrick brick = new MyBrick(MyBrick.brickWidth * i,(HEIGHT / 3) - j * MyBrick.brickHeight);
                        g2d.setColor(color[j]); //brick color.
                        g2d.fill(brick);
                        g2d.setColor(Color.BLACK); //frame brick color.
                        g2d.setStroke(new BasicStroke(5));
                        g2d.draw(brick);
        }}
        //Draw a pedal and color should be gray.
        MyPedal pedal = new MyPedal(WIDTH / 2 - (MyPedal.pedalWidth / 2) ,HEIGHT - MyPedal.pedalHeight);
        g2d.setColor(Color.GRAY);
        g2d.fill(pedal);
        //Draw a ball on top of the pedal. The color should be white. 
        MyBall ball = new MyBall(WIDTH / 2 - (MyBall.diameter / 2), HEIGHT  - (MyBall.diameter)- (MyPedal.pedalHeight));
        g2d.setColor(Color.WHITE);
        g2d.fill(ball);
    }
}
