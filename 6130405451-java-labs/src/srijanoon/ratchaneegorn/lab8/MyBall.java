package srijanoon.ratchaneegorn.lab8;
import java.awt.geom.Ellipse2D;

/** 
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 25 Mar 19
 */
public class MyBall extends Ellipse2D.Double{
        //one protected final static int variable called diameter. diameter should be set to 30.
	protected static int diameter = 30;
        //one constructor that accepts 2 parameters then call superclass constructor.
	protected MyBall(int x,int y) {
		super(x, y, diameter, diameter);
		
	}
}