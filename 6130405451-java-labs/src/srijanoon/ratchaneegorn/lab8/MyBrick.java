package srijanoon.ratchaneegorn.lab8;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

/**
 * 
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 25 Mar 19
 */
public class MyBrick extends Rectangle2D.Double{
    //2 protected final static int variables for width and height of a brick.
    protected static int brickWidth = 80;
    protected static int brickHeight = 20;
    //The constructor that accepts two parameters then call superclass.
    protected MyBrick(int x,int y){
        super(x, y, brickWidth, brickHeight);
}
    
}
   