package srijanoon.ratchaneegorn.lab7;
import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;
import srijanoon.ratchaneegorn.lab6.MobileDeviceFormV3;
/**
 *the program called MobileDeviceFormV4  which extends from MobileDeviceFormV3.  
 *Use the given image file or find any image file to illustrate the icon for menu “New”
 * and add sub menus to menuColor and menuSize.
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: Mar 4 2019
 */
public class MobileDeviceFormV4 extends MobileDeviceFormV3{
    protected JMenuItem subCred;
    protected JMenuItem subCgreen;
    protected JMenuItem subCblue;
    protected JMenuItem subS16;
    protected JMenuItem subS20;
    protected JMenuItem subS24;
    protected ImageIcon newIcon;
    public MobileDeviceFormV4(String string) {
        super(string);
    }
    protected void updateMenuIcon() {
        newIcon = new ImageIcon("images/new.jpg"); //add new icon to "New" menu.
        newItem.setIcon(newIcon);
    }
    protected void addSubMenu() {
        //Set and add sub Menu to menu "Color".
        subCred = new JMenuItem("Red");
        subCgreen = new JMenuItem("Green");
        subCblue = new JMenuItem("Blue");
        colorItem.add(subCred);
        colorItem.add(subCgreen);
        colorItem.add(subCblue);
        //Set and add sub Menu to menu "Size".
        subS16 =  new JMenuItem("16");
        subS20 = new JMenuItem("20");
        subS24 = new JMenuItem("24");
        sizeItem.add(subS16);
        sizeItem.add(subS20);
        sizeItem.add(subS24);
    }
    protected void addMenu(){
        super.addMenu();
        updateMenuIcon();
        addSubMenu();
    }
    protected void setFrameFeatures() {
        super.setFrameFeatures();
        this.setTitle("Mobile Device Form V4");  //Set title.
    }
    public static void createAndShowGUI(){
        MobileDeviceFormV4 mobileDeviceFormV4 = new MobileDeviceFormV4("Mobile Device Form V4");
        mobileDeviceFormV4.addComponents();
        mobileDeviceFormV4.addMenu();
        mobileDeviceFormV4.setFrameFeatures();
        mobileDeviceFormV4.setVisible(true);  
    }
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                createAndShowGUI();
            }
        });
    }

    
}