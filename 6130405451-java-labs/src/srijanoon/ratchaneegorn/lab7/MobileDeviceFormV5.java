package srijanoon.ratchaneegorn.lab7;
import java.awt.*;
import javax.swing.SwingUtilities;
/**
 * the program called MobileDeviceFormV5  which extends from MobileDeviceFormV4. 
 * Set the font of all labels to "Serif", plain, and size 14 and color of components.
 * Set the foreground color of “Cancel” button to red and “OK” button to blue.
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: Mar 4 2019
 */
public class MobileDeviceFormV5 extends MobileDeviceFormV4{
    public MobileDeviceFormV5(String string) {
        super(string);
    }
    protected void addComponents(){
        super.addComponents();
        //Set the foreground color button.
        buttonCancel.setForeground(Color.RED);
        buttonOk.setForeground(Color.BLUE);
    }
    protected void initComponents(){
        //Set font to all label.
        labelBrand.setFont(new Font("Serif", Font.PLAIN, 14));
        labelModel.setFont(new Font("Serif", Font.PLAIN, 14));
        labelWeight.setFont(new Font("Serif", Font.PLAIN, 14));
        labelPrice.setFont(new Font("Serif", Font.PLAIN, 14));
        labelmobileOS.setFont(new Font("Serif", Font.PLAIN, 14));
        //Set font to all textfield.
        textBrand.setFont(new Font("Serif", Font.PLAIN, 14));
        textModel.setFont(new Font("Serif", Font.PLAIN, 14));
        textWeight.setFont(new Font("Serif", Font.PLAIN, 14));
        textPrice.setFont(new Font("Serif", Font.PLAIN, 14));
        textReview.setFont(new Font("Serif", Font.PLAIN, 14));
    }
    protected void setFrameFeatures() {
        super.setFrameFeatures();
        this.setTitle("Mobile Device Form V5"); //Set title.
    }
    public static void createAndShowGUI(){
        MobileDeviceFormV5 mobileDeviceFormV5 = new MobileDeviceFormV5("Mobile Device Form V5");
        mobileDeviceFormV5.addComponents();
        mobileDeviceFormV5.addMenu();
        mobileDeviceFormV5.setFrameFeatures();
        mobileDeviceFormV5.setVisible(true);  
    }
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                createAndShowGUI();
            }
        });
    }
}
