package srijanoon.ratchaneegorn.lab7;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.SwingUtilities;
/**
 * the program called MobileDeviceFormV6  which extends from MobileDeviceFormV5. 
 * Add the image panel to the program.
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: Mar 4 2019
 */
public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
    protected JPanel imagePanel = new JPanel();
    protected JLabel imageLabel;
    protected Icon imageShow;
    public MobileDeviceFormV6(String string) {
        super(string);
    }
    @Override
    protected void addComponents(){
        super.addComponents();
        ReadImage image = new ReadImage();
        reviewPanel.add(image, BorderLayout.SOUTH); //Set picture locate.
    }
    @Override
    protected void setFrameFeatures() {
        super.setFrameFeatures();
        this.setTitle("Mobile Device Form V6"); //Set title.
    }
    public static void createAndShowGUI(){
        MobileDeviceFormV6 mobileDeviceFormV6 = new MobileDeviceFormV6("Mobile Device Form V6");
        mobileDeviceFormV6.addComponents();
        mobileDeviceFormV6.addMenu();
        mobileDeviceFormV6.setFrameFeatures();
        mobileDeviceFormV6.setVisible(true);  
    }
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run(){
                createAndShowGUI();
            }
        });
    }
    //Create method to read image.
    public class ReadImage extends JPanel {
        BufferedImage image;
        String imageName = "images/galaxyNote9.jpg";
        @Override
        protected void paintComponent(Graphics g){
            g.drawImage(image, 0,0, null); 
        }
        protected ReadImage(){
           try{
               image = ImageIO.read(new File(imageName));
           } catch(IOException shower){
               shower.printStackTrace(System.err);
           }
        }
        public Dimension getPreferredSize(){
            if(image == null)
                return new Dimension(100,100); //Set size picture.
            else
                return new Dimension(image.getWidth(),image.getHeight());    
        }
    }
}
