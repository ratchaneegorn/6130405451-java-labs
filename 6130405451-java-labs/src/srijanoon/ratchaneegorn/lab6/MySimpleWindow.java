package srijanoon.ratchaneegorn.lab6;
import java.awt.BorderLayout;
import javax.swing.*;
/**
 * MySimpleWindow to be a superclass for all other classes with these
 * requirements. This program MySimpleWindow extends from class JFrame.
 *
 * @author Ratchaneegorn Srijanoon ID:613040545-1 sec : 1 Date: Feb 25 2019
 */
public class MySimpleWindow extends JFrame {
    protected JButton buttonCancel = new JButton("Cancel");
    protected JButton buttonOk = new JButton("OK");
    protected JPanel pbutton = new JPanel();
    protected JPanel allpanel = new JPanel();
    protected JPanel onpanel = new JPanel();
    protected JPanel centerpanel = new JPanel();
    protected JPanel underpanel = new JPanel();

    public MySimpleWindow(String string) {
        // TODO Auto-generated constructor stub	
    }
   
        protected void addComponents() {
                pbutton.add(buttonCancel);
                pbutton.add(buttonOk);
                underpanel.setLayout(new BorderLayout());
                allpanel.setLayout(new BorderLayout());
                underpanel.add(pbutton, BorderLayout.SOUTH);
                allpanel.add(underpanel, BorderLayout.SOUTH);
                
            }
	protected void setFrameFeatures() {
		this.setTitle("My Simple Window");
		this.add(allpanel);
                pack();
                this.setLocationRelativeTo(null);
                this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 }
    public static void createAndShowGUI() {
            MySimpleWindow msw = new MySimpleWindow("My Simple Window");
            msw.addComponents();
            msw.setFrameFeatures();
            msw.setVisible(true);
	}
	
    public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});

	 }
}
