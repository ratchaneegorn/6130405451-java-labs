package srijanoon.ratchaneegorn.lab6;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *the program called MobileDeviceFormV3  which extends from MobileDeviceFormV2 
 * and whose interface look like the program shown in Figure 7-8.
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: Feb 25 2019
 */
public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
    protected JFrame windowFrame;
    protected JLabel labelFeatures = new JLabel("Features:");
    protected JPanel panelFeatures = new JPanel();
    protected List listFeatures = new List(4);
    //Menus
    protected JMenuBar menuBar = new JMenuBar();
    protected JMenu menuFile;
    protected JMenu menuConfig;
    protected JMenuItem newItem;
    protected JMenuItem openItem;
    protected JMenuItem saveItem;
    protected JMenuItem exitItem;
    protected JMenu colorItem;
    protected JMenu sizeItem ;
    @Override
    protected void addComponents() {
        listFeatures.add("Design and build quality");
        listFeatures.add("Great Camera");
        listFeatures.add("Screen");
        listFeatures.add("Battery Life");
        panelFeatures.setLayout(new BorderLayout());
        panelFeatures.setLayout(new GridLayout(0,2));
        panelFeatures.add(labelFeatures,BorderLayout.WEST);
        panelFeatures.add(listFeatures,BorderLayout.EAST);
        super.addComponents();
        underpanel.add(panelFeatures,BorderLayout.NORTH);
    }
    protected void addMenu(){
        menuBar = new JMenuBar();//JMenuBar 
        newItem = new JMenuItem("New");
        openItem = new JMenuItem("Open");
        saveItem = new JMenuItem("Save");
        exitItem = new JMenuItem("Exit");
        colorItem = new JMenu("Color");
        sizeItem = new JMenu("Size");
        //Menu “File” has four menu items which are “New”, “Open”, “Save”, and “Exit”
        menuFile = new JMenu("File");
        menuBar.add(menuFile);
        menuFile.add(newItem);
        menuFile.add(openItem);
        menuFile.add(saveItem);
        menuFile.add(exitItem);
        //Menu “Config” has two menu items which are “Color”, and “Size”
        menuConfig = new JMenu("Config");
        menuBar.add(menuConfig);
        menuConfig.add(colorItem);
        menuConfig.add(sizeItem);
        this.setJMenuBar(menuBar); //Set menuBar
        
    }
    public MobileDeviceFormV3(String string) {
        super(string);
    }
    @Override
    protected void setFrameFeatures() {  
		super.setFrameFeatures();
                this.setTitle("Mobile Device Form V3");
	 }
    public static void createAndShowGUI() {
            MobileDeviceFormV3 mobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3"); //title window
            mobileDeviceFormV3.addComponents();
            mobileDeviceFormV3.addMenu();
            mobileDeviceFormV3.setFrameFeatures();
            mobileDeviceFormV3.setVisible(true);
	}
    public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
                @Override
		public void run() {
			createAndShowGUI();
		}
	});  
}
}
