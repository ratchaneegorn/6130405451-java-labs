package srijanoon.ratchaneegorn.lab6;
import java.awt.*;
import javax.swing.*;
/** 
 * the program called MobileDeviceFormV1 which extends from MySimpleWindow 
 * and  whose interface looks like the program shown in Figure 3.
 * 1)The left part contains JLabel and then 
 * 2)the right part contains JTextField which its length is 15.
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: Feb 25 2019
 */
public class MobileDeviceFormV1 extends MySimpleWindow{
    protected JPanel rbuttonPanel = new JPanel();
    protected JPanel brandPanel = new JPanel();
    protected JPanel modelPanel = new JPanel();
    protected JPanel weightPanel = new JPanel();
    protected JPanel pricePanel = new JPanel();
    protected JPanel mobilePanel = new JPanel();
    protected JPanel buttonCOPanel = new JPanel();
    //The label which it is set. 
    protected JLabel labelBrand = new JLabel("Brand name:");
    protected JLabel labelModel= new JLabel("Model name:");
    protected JLabel labelWeight = new JLabel("Weight (kg.):");
    protected JLabel labelPrice = new JLabel("Price (Baht):");
    protected JLabel labelmobileOS = new JLabel("Mobile OS:");
    //JTextField which its length is 15.
    protected JTextField textBrand = new  JTextField(15);
    protected JTextField textModel = new  JTextField(15);
    protected JTextField textWeight = new  JTextField(15);
    protected JTextField textPrice = new  JTextField(15);
    //Use class ButtonGroup to group radio buttons and allow a user to choose only one radio button at a time.
    protected JRadioButton rbuttonAndroid = new JRadioButton("Android"); 
    protected JRadioButton rbuttonIOS = new JRadioButton("iOS"); 
    protected ButtonGroup grouprbutton = new ButtonGroup();
    
    public MobileDeviceFormV1(String string) {
        super(string);
    }
    @Override
    protected void addComponents() {
            //Add redio button to group redio button
            grouprbutton.add(rbuttonAndroid);
            grouprbutton.add(rbuttonIOS);
            rbuttonPanel.add(rbuttonAndroid);
            rbuttonPanel.add(rbuttonIOS);
            //Add label and textfield to panel and set locate.
            brandPanel.setLayout(new BorderLayout());
            brandPanel.setLayout(new GridLayout(0,2));
            modelPanel.setLayout(new BorderLayout());
            modelPanel.setLayout(new GridLayout(0,2));
            weightPanel.setLayout(new BorderLayout());
            weightPanel.setLayout(new GridLayout(0,2));
            pricePanel.setLayout(new BorderLayout());
            pricePanel.setLayout(new GridLayout(0,2));
            mobilePanel.setLayout(new BorderLayout());
            mobilePanel.setLayout(new GridLayout(0,2));
            rbuttonPanel.setLayout(new BorderLayout());
            rbuttonPanel.setLayout(new GridLayout(0,2));
            
            brandPanel.add(labelBrand,BorderLayout.WEST);
            brandPanel.add(textBrand,BorderLayout.EAST);
            modelPanel.add(labelModel,BorderLayout.WEST);
            modelPanel.add(textModel,BorderLayout.EAST);
            weightPanel.add(labelWeight,BorderLayout.WEST);
            weightPanel.add(textWeight,BorderLayout.EAST);
            pricePanel.add(labelPrice,BorderLayout.WEST);
            pricePanel.add(textPrice,BorderLayout.EAST);
            mobilePanel.add(labelmobileOS,BorderLayout.WEST);
            mobilePanel.add(rbuttonPanel,BorderLayout.EAST);
            buttonCOPanel.add(buttonCancel,BorderLayout.CENTER);
            buttonCOPanel.add(buttonOk,BorderLayout.CENTER);
            //Add all panel to one panel.
            onpanel.add(brandPanel);
            onpanel.add(modelPanel);
            onpanel.add(weightPanel);
            onpanel.add(pricePanel);
            onpanel.add(mobilePanel);
            onpanel.setLayout(new GridLayout(5,1));//Set layout of the window
            underpanel.setLayout(new BorderLayout());
            underpanel.add(buttonCOPanel, BorderLayout.SOUTH);
            allpanel.setLayout(new BorderLayout());
            allpanel.add(onpanel, BorderLayout.NORTH);
            allpanel.add(underpanel, BorderLayout.SOUTH);
	}
    @Override
    protected void setFrameFeatures() {
                super.setFrameFeatures();
                this.setTitle("Mobile Device Form V1"); //set title.
	 }
    public static void createAndShowGUI() {
        MobileDeviceFormV1 mobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1"); //title window
        mobileDeviceFormV1.addComponents();
        mobileDeviceFormV1.setFrameFeatures();  
        mobileDeviceFormV1.setVisible(true);
}
    public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});  
    }
}
