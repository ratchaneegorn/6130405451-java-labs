package srijanoon.ratchaneegorn.lab6;
import java.awt.*;
import javax.swing.*;
/**
 *  the program called MobileDeviceFormV2  which extends from MobileDeviceFormV1 
 * and whose interface look like the program shown in Figure 5.
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: Feb 25 2019
 */
public class MobileDeviceFormV2 extends MobileDeviceFormV1{
        protected JLabel labelType = new JLabel("Type:");
        protected JLabel labelReview = new JLabel("Review:");
        protected JComboBox comboType = new JComboBox();
        protected JTextArea textReview = new JTextArea(2,35); 
        protected JScrollPane scrollText = new JScrollPane(textReview);
        protected JPanel pComboType = new JPanel();
        protected JPanel reviewPanel = new JPanel();
        protected JPanel centerpanel = new JPanel(); 
    protected void addComponents(){
        //the combo box of device type which includes “Phone”, “Tablet”, “Smart TV” 
        pComboType.setLayout(new BorderLayout());//Set layout
        pComboType.add(labelType,BorderLayout.WEST);
        pComboType.add(comboType,BorderLayout.EAST);
        pComboType.setLayout(new GridLayout(0,2));
        pComboType.add(labelType);
        pComboType.add(comboType);
        comboType.addItem("Phone");
        comboType.addItem("Tablet");
        comboType.addItem("Smart TV");
        reviewPanel.add(labelReview);
        reviewPanel.add(scrollText);
        reviewPanel.setLayout(new BorderLayout()); //Set layout
        reviewPanel.add(labelReview, BorderLayout.WEST);
        reviewPanel.add(scrollText, BorderLayout.SOUTH); 
        scrollText.getViewport().add(textReview);       
        scrollText.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); 
        textReview.setLineWrap(true);
        textReview.setWrapStyleWord(true);
        textReview.setText("Bigger than previous Note phones in every way,");
        textReview.append("the Samsung Galaxy Note 9 has a larger 6.4-inch screen,");
        textReview.append("heftier 4,000mAh battery, and a massive 1TB of storage option.");
        //Add all panel to one panel.
        super.addComponents();
        centerpanel.setLayout(new BorderLayout());
        centerpanel.add(pComboType, BorderLayout.NORTH);
        underpanel.setLayout(new BorderLayout());
        underpanel.add(reviewPanel, BorderLayout.CENTER);
        underpanel.add(buttonCOPanel, BorderLayout.SOUTH);
        
        allpanel.setLayout(new BorderLayout());
        allpanel.add(onpanel, BorderLayout.NORTH);
        allpanel.add(centerpanel, BorderLayout.CENTER);
        allpanel.add(underpanel, BorderLayout.SOUTH);
    }
    public MobileDeviceFormV2(String string) {
        super(string);   
    }
        @Override
    protected void setFrameFeatures() {
        super.setFrameFeatures();
        this.setTitle("Mobile Device Form V2");
	 }
    public static void createAndShowGUI() {
            MobileDeviceFormV2 mobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2"); //title window
            mobileDeviceFormV2.addComponents();
            mobileDeviceFormV2.setFrameFeatures();
            mobileDeviceFormV2.setVisible(true);
	}
    public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});  
    }    
   
}