package srijanoon.ratchaneegorn.lab2;
/*
*This is java programe that accepts 4 arguments of money you have and Compute how much money you have. 
*You will input the number of notes of 1,000 Baht, 500 Baht, 100 Baht and 20 Baht.
*the program should output the total amount of money you have.
*the program is in the format
*Total Money is:<total amount of money you have>
*if Error message, output when the number of argument is not four.
*ComputeMoney <1,000 Baht><500 Baht><100 Baht><20 Baht>
* Author: Ratchaneegorn Srijanoon
* ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
**/
import java.util.Scanner;
public class ComputeMoney {
    public static void main(String[] args) { 
        if(args.length != 4){
             System.err.println("ComputeMoney <1,000 Baht> <500 Baht> <100 Baht> <20 Baht>"); 
        }
        else{
            Scanner calculate = new Scanner(System.in);
            int number1, number2, number3, number4, sumMoney1000, sumMoney500, sumMoney100, sumMoney20, totalMoney; 
            number1 = calculate.nextInt();
            number2 = calculate.nextInt();
            number3 = calculate.nextInt();
            number4 = calculate.nextInt();
            sumMoney1000 = 1000 * number1;
            sumMoney500 = 500 * number2;
            sumMoney100 = 100 * number3;
            sumMoney20 = 20 * number4;
            totalMoney = sumMoney1000+sumMoney500+sumMoney100+sumMoney20;
            System.out.println("Total Money is:" + totalMoney ); 
         }
         } 
    }
    
    

    
    

