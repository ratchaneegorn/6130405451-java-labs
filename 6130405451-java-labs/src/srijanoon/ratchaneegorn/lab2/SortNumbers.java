package srijanoon.ratchaneegorn.lab2;
import java.util.Arrays;
import java.util.Scanner;

/**This is java programe that accept 4 decimals, read and store
*those numbers in an array, and then use Java subroutine to sort these numbers.
*  Author: Ratchaneegorn Srijanoon
* ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
 */
public class SortNumbers {
    public static void main(String[] args) { 
       Scanner inputNumbers = new Scanner(System.in);
       int number = inputNumbers.nextInt();
       int[] numbersort = {number};
       Arrays.sort(numbersort);
       for(int i = 0;i < numbersort.length; i++){
           System.out.print(numbersort[i]);
       }
      
       
        
    }
}
