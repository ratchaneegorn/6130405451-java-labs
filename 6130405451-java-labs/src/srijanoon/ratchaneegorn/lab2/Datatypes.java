package srijanoon.ratchaneegorn.lab2;
/**
* This Java program that declare following variable in the given types and conditions
* the program is in the format
* My name is + <fristname lastname>
* My student ID is + <student id>
* <1><2><3><4>
* <the last two digits of IDNumber(declare a variable with a long value)>
* <the floating point of the First two digits of IDNumber><the double point of the First two digits of IDNumber>
* so 1 is first Letter of firstname
* 	 2 is boolean variable with value is true
* 	 3 is last two digits id number declare a variable with an octal number
* 	 4 is last two digits id number declare a variable with an hexadecimal number
*
* Author: Ratchaneegorn Srijanoon
* ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
**/

public class Datatypes {

	public static void main(String[] args) {
		String myName = new String("Ratchaneegorn Srijanoon");	
		String myID = new String("6130405451");
		char fristLetterOfFirstName = 'R';
		boolean nameTrue = true;
		int theLastTwoDigitsOfYourlIDNumberOct = 063;//hex 33,dec 51,oct 63
		int theLastTwoDigitsOfYourlIDNumberHex = 0x33;
		float theLastTwoDigitsOfYourIDNumberfloat = 51.61f;
		double theLastTwoDigitsOfYourIDNumberdouble = 61.51d;
		
		System.out.println("My name is " + myName);
		System.out.println("My student ID is " + myID);
		System.out.print(fristLetterOfFirstName + " ");
		System.out.print(nameTrue + " ");
		System.out.println(theLastTwoDigitsOfYourlIDNumberOct + " " + theLastTwoDigitsOfYourlIDNumberHex);
		
		System.out.println(theLastTwoDigitsOfYourlIDNumberOct+" "+theLastTwoDigitsOfYourIDNumberfloat+ " " +theLastTwoDigitsOfYourIDNumberdouble);
		
	}

}
