package srijanoon.ratchaneegorn.lab2;
import java.util.Scanner;
 /**This is java programe that accepts one input argument (your school name) and save that argument in the variable called schoolName 
 * and then use the method of class String to check whether 
 * the variable schoolName has the substring “college” (ignore cases) and “university” (ignore cases)
 *display output in format
 * <yourinput> is 'university' or 'college' or 'neither a university nor a college '
 * Author: Ratchaneegorn Srijanoon
* ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
 **/
public class StringAPI {
    public static void main(String[] args) {
            Scanner inputSchoolName = new Scanner(System.in);
            String schoolName = inputSchoolName.nextLine();
            String checkUniversity = "University";
            String checkColleage = "Colleage"; 	
		if (schoolName.contains(checkUniversity)) {
			System.out.println(schoolName + " is a university" );
		} 
		else if (schoolName.contains(checkColleage)) {
			System.out.println(schoolName + " is a colleage");
		}
		else {
			System.out.println(schoolName+ " is neither a university nor a colleage");
		}
	}
}

