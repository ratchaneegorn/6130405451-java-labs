package srijanoon.ratchaneegorn.lab2;
/**
 * This Java program that accepts 3 arguments: your favorite football player, the
*  football club that the player plays for, and the nationality of that player. The output of
*  the program is in the format
* "My favorite football player is " + <footballer name>
* "His nationality is " + <nation>
* "He plays for " + <football club>
*
* Author: Ratchaneegorn Srijanoon
* ID: 613040545-1
* Sec: 1
* Date: January 21, 2019
*
**/

public class Footballer {
	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("Usage:Footballer<footballer name><nationality><club name>");
			System.exit(1);
		} else {
			System.out.println("My favorite football player is " + args[0]);
			System.out.println("His nationality is " + args[1]);
			System.out.println("He plays for " + args[2]);

		}
	}

}
