package srijanoon.ratchaneegorn.lab9;
import srijanoon.ratchaneegorn.lab8.MyCanvas;
import srijanoon.ratchaneegorn.lab8.MyPedal;
/*
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyPedalV2 extends MyPedal {
    //declare speedPedal
    protected static int speedPedal = 20;
    public MyPedalV2(int x, int y) {
        super(x,y);
    }
    //the code for moveLeft()
    public void moveLeft(){
        if(x - speedPedal < 0){
            x = 0;
        }else{
            x -= speedPedal;
        }
    }
    //the code for moveRight()
    public void moveRight(){
        if(x + speedPedal > MyCanvas.WIDTH - MyPedal.pedalWidth ){
            x = MyCanvas.WIDTH - MyPedal.pedalWidth;
        }else{
            x += speedPedal;
        }
    }
}
