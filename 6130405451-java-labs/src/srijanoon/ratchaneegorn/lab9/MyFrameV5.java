package srijanoon.ratchaneegorn.lab9;
import javax.swing.SwingUtilities;
import srijanoon.ratchaneegorn.lab8.MyFrameV4;
/**
 * MyFrameV5 which has the following properties.
 * Extends MyFrameV4 from lab8 has main method as shown in Figure 1.
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyFrameV5 extends MyFrameV4{
    
    public MyFrameV5(String text) {
        super(text);
    }
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                createAndShowGUI();
            }
        });
    }
    public static void createAndShowGUI(){
        MyFrameV5 msw = new MyFrameV5("My Frame V5");
        msw.addComponents();
        msw.setFrameFeatures();
    }
    protected void addComponents(){
        add(new MyCanvasV5());
    }
}
