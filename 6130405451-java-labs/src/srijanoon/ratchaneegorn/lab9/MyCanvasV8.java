package srijanoon.ratchaneegorn.lab9;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import srijanoon.ratchaneegorn.lab8.MyBall;
import srijanoon.ratchaneegorn.lab8.MyBrick;
import srijanoon.ratchaneegorn.lab8.MyCanvas;
import srijanoon.ratchaneegorn.lab8.MyPedal;
/*
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyCanvasV8 extends MyCanvasV7 implements Runnable,KeyListener {
    protected int numCol = super.WIDTH / MyBrick.brickWidth;
    protected int numRow = 7;
    protected int numVisibleBricks = numCol * numRow;
    protected MyBrickV2[][] bricks;
    protected Thread running;
    protected Color[] color = {Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN,
                Color.YELLOW, Color.ORANGE, Color.RED};
    protected MyPedalV2 pedal;
    protected int lives;
    public MyCanvasV8(){
        //initializebricka with numRow and numCol
        bricks = new MyBrickV2[numRow][numCol];
        
        //initialize ball with
        ball = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.diameter / 2,MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
        ball.ballVelX = 0;
        ball.ballVelY = 0;
        running = new Thread(this);
        for(int i = 0; i < numRow; i++){
            for(int j = 0; j < numCol; j++){
                bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, super.HEIGHT / 3 - i * MyBrick.brickHeight);
            }
        }
        running.start();
        setFocusable(true);
        addKeyListener((KeyListener) this);
        pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.pedalWidth / 2,MyCanvas.HEIGHT - MyPedal.pedalHeight);
        lives = 3;
    }
    public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
                for(int i = 0; i < numRow;i++){
                    for(int j = 0; j < numCol; j++){
                        if(bricks[i][j].visible){
                            g2d.setColor(color[i]);
                            g2d.fill(bricks[i][j]);
                            g2d.setStroke(new BasicStroke(4));
                            g2d.setColor(Color.BLACK);
                            g2d.draw(bricks[i][j]);
                        }
                    }
                }
                g2d.setColor(Color.WHITE);
		g2d.fill(ball);
                g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
                g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
                g2d.setColor(Color.BLUE);
                String s = "Lives :" + lives;
                g2d.drawString(s, 10, 30);
                 if(numVisibleBricks == 0){
                    g2d.setColor(Color.GREEN);
                    g2d.drawString("YOU WON", 350, MyCanvas.HEIGHT / 2);
                } 
                if(lives == 0){
                    g2d.setColor(Color.GRAY);
                    g2d.drawString("GAME OVER", 350, MyCanvas.HEIGHT / 2);
                }
               
    }
    public void checkPassBottom(){
        if(ball.y >= MyCanvas.HEIGHT){
            ball.x = pedal.x + MyPedal.pedalWidth / 2 - MyBall.diameter / 2;
            ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
            ball.ballVelX = 0;
            ball.ballVelY = 0;
            lives --;
            repaint();
        }
    }
    public void checkCollision(MyBallV2 ball,MyBrickV2 brick){
        double x = ball.x + MyBall.diameter / 2.0;
        double y = ball.y + MyBall.diameter / 2.0;
        double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
        double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));
        boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
        if(collided){
            if(deltaX * deltaX < deltaY * deltaY){
                ball.ballVelY *= -1;
            }else{
                ball.ballVelX *= -1;
            }
            ball.move();;
            brick.visible = false;
            numVisibleBricks --;
        }  
    }
    public void collideWithPedal(){
        double x = ball.x + MyBall.diameter / 2.0;
        double y = ball.y + MyBall.diameter / 2.0;
        double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyBrick.brickWidth));
        double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyBrick.brickHeight));
        boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
        if(collided){
            if(deltaX * deltaX < deltaY * deltaY){
                ball.ballVelY *= -1;
            }else{
                ball.ballVelX *= -1;
            }
            ball.move();
        }   
    }
    public void run(){
        while(true){
            if(ball.x <= 0 || ball.x >= MyCanvas.WIDTH - MyBall.diameter){
                ball.ballVelX *= -1;
            }
            if(ball.y <= 0){
                ball.ballVelY *=-1;
            }
            for(int i = 0; i < numRow;i++){
                for(int j = 0; j < numCol; j++){
                    if(bricks[i][j].visible){
                        checkCollision(ball, bricks[i][j]);
                    }
                }
            }
            collideWithPedal();
            checkPassBottom();
            ball.move();
            repaint();
            try{
                Thread.sleep(20);
            }catch(InterruptedException ex){
            }
            if(lives == 0){
                break;
            }else if(numVisibleBricks == 0){
                break;
            }
        }
    } 
    public void keyPressed(KeyEvent arg0){
        switch (arg0.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                pedal.moveLeft();
                break;
            case KeyEvent.VK_RIGHT:
                pedal.moveRight();
                break;
            case KeyEvent.VK_SPACE:
                ball.ballVelY = 4;
                int random = (int) (Math.random() * 1) + 1;
                if(random == 1){
                    ball.ballVelX = 4;
                }else if(random == 2){
                    ball.ballVelX = -4;
                }
                break;
            default:
                break;
        }
    }
    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        
    }
}
