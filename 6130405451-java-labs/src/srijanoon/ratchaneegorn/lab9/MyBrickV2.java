package srijanoon.ratchaneegorn.lab9;

import srijanoon.ratchaneegorn.lab8.MyBrick;

/*
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyBrickV2 extends MyBrick {
    protected boolean visible;
    public MyBrickV2(int x, int y) {
        super(x, y);
        visible = true ;
    }
    
}
