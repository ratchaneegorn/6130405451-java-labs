package srijanoon.ratchaneegorn.lab9;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import srijanoon.ratchaneegorn.lab8.*;
/*
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyCanvasV5 extends MyCanvasV4 implements Runnable {
    MyBallV2 ball = new MyBallV2(0, HEIGHT / 2 - MyBall.diameter / 2);
    Thread running = new Thread(this);
    public MyCanvasV5(){
        super();
        ball.ballVelX = 2;
        ball.ballVelY = 0;
        running.start();
    }
    public void run(){
        while(true){
            //stop the ball when hit the right side of the wall.
            if(ball.x > super.WIDTH - MyBall.diameter){
               break;
            }
            //move the ball
            ball.move();
            repaint();
            //Delay
            try{
                Thread.sleep(10);
            } 
            catch(InterruptedException ex){   
            }}
        }
     public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        
        g2d.setColor(Color.BLACK);
        g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
        //Draw a ball
        g2d.setColor(Color.WHITE);
        g2d.fill(ball);
    }

}
