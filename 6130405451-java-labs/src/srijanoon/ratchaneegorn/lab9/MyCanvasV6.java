package srijanoon.ratchaneegorn.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import srijanoon.ratchaneegorn.lab8.MyBall;
import srijanoon.ratchaneegorn.lab8.MyCanvas;

/*
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
    MyBallV2 ball2 = new MyBallV2(super.WIDTH / 2 - MyBall.diameter / 2, super.HEIGHT / 2 - MyBall.diameter / 2 );
    Thread running = new Thread(this);
    public MyCanvasV6(){
        super();
        ball2.ballVelX = 1;
        ball2.ballVelY = 1;
        running.start();
    }
    public void run(){
      while(true){
          //bounce the ball when hit the right or the left side of the wall.
            if(ball2.x < 0 || ball2.x + MyBall.diameter >= WIDTH){
                ball2.ballVelX *= -1;         
            }
          //bouncing the ball when it hit the top and bottom of the wall.
            if(ball2.y < 0 || ball2.y + MyBall.diameter >= HEIGHT){
                ball2.ballVelY *= -1;
            }
            //move the ball
            ball2.move();
            repaint();
            //Delay
            try{
                Thread.sleep(10);
            }
            catch(InterruptedException ex){
            }
      } 
    }
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.setColor(Color.BLACK);
        g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
        g2d.setColor(Color.WHITE);
        g2d.fill(ball2);
    }
}
