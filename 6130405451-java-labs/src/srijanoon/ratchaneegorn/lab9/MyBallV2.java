package srijanoon.ratchaneegorn.lab9;
import srijanoon.ratchaneegorn.lab8.MyBall;
/*
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyBallV2 extends MyBall{
    protected int ballVelX;
    protected int ballVelY;


    public MyBallV2(int x, int y) {
        super(x, y);
    }

   public void move() {
        this.x += this.ballVelX;
        this.y += this.ballVelY; 
    }
    
    
}
