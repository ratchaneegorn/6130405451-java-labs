package srijanoon.ratchaneegorn.lab9;
import javax.swing.SwingUtilities;
/*
 * @author Ratchaneegorn Srijanoon
 * ID 613040545-1
 * Sec 1
 * Date 8 Apr 19
 */
public class MyFrameV6 extends MyFrameV5{    
    public MyFrameV6(String text) {
        super(text);
    }
     public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                createAndShowGUI();
            }
        });
    }
    public static void createAndShowGUI(){
        MyFrameV6 msw = new MyFrameV6("My Frame V6");
        msw.addComponents();
        msw.setFrameFeatures();
    }
    protected void addComponents(){
        add(new MyCanvasV6());
    }
}
