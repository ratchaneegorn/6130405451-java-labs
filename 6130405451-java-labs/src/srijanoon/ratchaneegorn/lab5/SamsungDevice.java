package srijanoon.ratchaneegorn.lab5;
/*
 * 
 * Author:Ratchaneegorn Srijanoon
 * ID: 613040545-1
 * sec: 1
 * Date: 18 Feb 2019
 */
public class SamsungDevice extends MobileDevice{
	private String modelName;
	private String os = "Android";
	private int price;
	private int weight;
	private static String brand = "Samsung";
	private double androidVertion;
	public SamsungDevice(String modelName, int price, double androidVertion ) {
		super(modelName, modelName, price);
		this.modelName = modelName;
		this.price = price;
		this.os = "Android";
		this.brand = brand;
		this.androidVertion = androidVertion;
	}
	public SamsungDevice(String modelName, int price, int weight, double androidVertion ) {
		super(modelName, modelName, price);
		this.modelName = modelName;
		this.price = price;
		this.weight = weight;
		this.brand = brand;
		this.androidVertion = androidVertion;
	}
	public double getAndroidVertion() {
		return androidVertion;
	}

	public void setAndroidVertion(double androidVertion) {
		this.androidVertion = androidVertion;
	}

	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public static String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String toString() {
		return "SamsungDevice [modelName= " + modelName + ", os= " + os + ", price= " + price + " Baht" + ", weight= " + weight + " g" + ", Android version: "+ androidVertion + "]";
	}	
}	