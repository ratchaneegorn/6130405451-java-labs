package srijanoon.ratchaneegorn.lab5;
 /**
  * The AndroidSmartWatch program that is a subclass of AndroidDevice.
  * Has private instance variables to record the following information: modelName,brandName, and price.
  * Has public methods to setter and getter methods for each attribute.
  * Author:Ratchaneegorn Srijanoon
  * ID: 613040545-1
  * sec: 1
  * Date: 18 Feb 2019
 */
public class AndroidSmartWatch extends AndroidDevice{
    //Private instance variables to record information.
        private String modelName;
        private String brandName;
        private int price;
    public AndroidSmartWatch(String modelName, String brandName, int price) {
	super();
        this.modelName = modelName;
        this.brandName = brandName;
	this.price = price;
    }
    public void usage() {
	System.out.println("AndroidSmartWathch Usage: Show time, date, your heart rate, and your step count");
	}
    @Override
    public String toString() {
        return "AndroidSmartWatch{" + "modelName=" + modelName + ", brandName=" + brandName + ", price=" + price + '}';
    }
    //A public methods to setter and getter methods. 
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getPrice() {
        return price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    //A method displayTime() that displays the output in format.
    public void displayTime(){
            System.out.println("Display time only using a digital format.");
        }
}

