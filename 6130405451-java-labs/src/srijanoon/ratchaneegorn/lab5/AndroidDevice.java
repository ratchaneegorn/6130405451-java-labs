package srijanoon.ratchaneegorn.lab5;
/**
 *The AndroidDevice program that satisfies these following
 * 3.1 AndroidDevice is an abstract class that has abstract method usage()
 * 3.2 Has the private static constant class member called os which its value is “Android”
 * 3.3 Has the public method to get the value of the static variable os
 * Author:Ratchaneegorn Srijanoon
 * ID: 613040545-1
 * sec: 1
 * Date: 18 Feb 2019
 */
public abstract class AndroidDevice{
    public abstract void usage();  //Abstract method name usage().
    private String os = "Android"; //Private class name os and value "Android".
    public String getOs() {
        return os;
    }
}