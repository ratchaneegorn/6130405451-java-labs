package srijanoon.ratchaneegorn.lab5;
/**
 * The AndroidDevices2019 which is to test class AndroidDevice and its subclass.
 * Its code and its output are shown in format.
 * 
 * Author:Ratchaneegorn Srijanoon
 * ID: 613040545-1
 * sec: 1
 * Date: 18 Feb 2019
 */
public class AndroidDevices2019 {
    public static void main(String[] args){
        AndroidDevice ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390) ;
        AndroidSmartWatch xiaomiMiBand3 = new AndroidSmartWatch("Xiaomi", "Mi Band 3", 950) ;
        ticwatchPro.usage();
        System.out.println(ticwatchPro);
        System.out.println(xiaomiMiBand3);   
    }
}
