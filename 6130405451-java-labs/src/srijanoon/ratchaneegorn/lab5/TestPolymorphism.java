package srijanoon.ratchaneegorn.lab5;
/**
 * The TestPolymorphism which is to test class AndroidSmartWatch and SamsungDevice.
 * Author:Ratchaneegorn Srijanoon
 * ID: 613040545-1
 * sec: 1
 * Date: 18 Feb 2019
 */
public class TestPolymorphism {
    public static void main(String[] args){
        AndroidSmartWatch ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
        SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500, 201, 8.1);
        System.out.println(ticwatchPro);
        ticwatchPro.displayTime();
        System.out.println();
        System.out.println(note9);
        note9.displayTime();
    }
}
