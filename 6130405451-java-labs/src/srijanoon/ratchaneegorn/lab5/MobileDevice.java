package srijanoon.ratchaneegorn.lab5;
/**
 * The program is MobileDevice that has private instance variables to record the following information: 
 * modelName, os, price and weigh.
 * Author:Ratchaneegorn Srijanoon
 * ID: 613040545-1
 * sec: 1
 * Date: 18 Feb 2019
 */
public class MobileDevice {
	//Create private instance variables to record the following information
			private String modelName;
			private String os;
			private int price;
			private int weight;
   //Create a public constructor which accepts values for the attributes modelName,os,price and weight  
	public MobileDevice(String modelName, String os, int price, int weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}
	public MobileDevice(String modelName, String os, int price) {
		super();
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = 0 ;	
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "MobileDevice [modelName=" + modelName + ", os=" + os + ", price=" + price + ", weight=" + weight + "]";
	}	
}



	