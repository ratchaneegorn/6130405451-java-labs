package srijanoon.ratchaneegorn.lab5;
/**
*The given class SamsungDevices2019 which is to test class SamsungDevice and its output are shown in display
* ------------------------
* I would like to have 
* SamsungDevice [modelName= Galaxy Note9 , os= Android, price= 25500 Baht, weight= 201 g, Android version: 8.1]
* But to save money, I should buy 
* SamsungDevice [modelName= Galaxy S9 , os= Android, price= 23900 Baht, weight= 163 g, Android version: 8.0]
* Samsung Galaxy Note 9 is cheaper than Samsung Galaxy s9 by 1600.0 Baht. 
* Both these device have the same brand which is Samsung.
* ------------------------
 * Author:Ratchaneegorn Srijanoon
 * ID: 613040545-1
 * sec: 1
 * Date: 18 Feb 2019
*/
public class SamsungDevices2019 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MobileDevice s9 = new SamsungDevice("Galaxy S9 ", 23900, 163, 8.0);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note9 ", 25500, 201, 8.1);
		System.out.println("I would like to have ");
		System.out.println(note9);
		System.out.println("But to save money, I should buy ");
		System.out.println(s9);
		double diff = note9.getPrice() - s9.getPrice();
		System.out.println("Samsung Galaxy Note 9 is cheaper than Samsung Galaxy s9 by " + diff + " Baht. ");
		System.out.println("Both these device have the same brand which is " + SamsungDevice.getBrand());
	}

}


