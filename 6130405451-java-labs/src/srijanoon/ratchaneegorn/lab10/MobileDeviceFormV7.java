package srijanoon.ratchaneegorn.lab10;
import java.awt.event.*;
import javax.swing.*;
import srijanoon.ratchaneegorn.lab7.MobileDeviceFormV6;

/**
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: 21 Apr 2019
 */
public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener, ItemListener{
    
    public MobileDeviceFormV7(String string) {
        super(string);
    }
   
    public void handleOKButton(){
       JOptionPane.showMessageDialog(null, String.format("Brand Name: "+textBrand.getText()+" Model Name: "+textModel.getText()+" Weight(kg.): "
               +textWeight.getText()+" Price(Baht): "+ textPrice.getText()+ "\n OS: "+ grouprbutton.getSelection().getActionCommand()
               +"\n Type :" + comboType.getSelectedItem() +"\nFeatures: "+ listFeatures.getSelectedItem()
               +"\nReview : "+textReview.getText()));
    }
    public void handleCancelButton(){
    	textBrand.setText("");
        textModel.setText("");
        textWeight.setText("");
        textPrice.setText("");
        textReview.setText("");
    }
    public void actionPerformed(ActionEvent event){
        Object src = event.getSource();
        if(src == buttonOk){
            handleOKButton();
        }else if(src == buttonCancel){
            handleCancelButton();
        }
   }
   protected void addListeners(){
	   super.addMenu();
	   buttonOk.addActionListener(this);
	   buttonCancel.addActionListener(this);
   }
   public static void createAndShowGUI(){
        MobileDeviceFormV7 mobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
        mobileDeviceFormV7.addComponents();
        mobileDeviceFormV7.addListeners();
        mobileDeviceFormV7.setFrameFeatures();
        mobileDeviceFormV7.setVisible(true);  
    }
   protected void setFrameFeatures() {
        super.setFrameFeatures();
        this.setTitle("Mobile Device Form V7"); //Set title.
    }
   public static void main(String[] args){
	        SwingUtilities.invokeLater(new Runnable(){
	            @Override
	            public void run(){
	                createAndShowGUI();
	            }
	        });
	 }

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		
	}
}