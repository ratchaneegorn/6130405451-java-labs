package srijanoon.ratchaneegorn.lab10;

import java.awt.event.*;
import javax.swing.*;

/**
 * @author Ratchaneegorn Srijanoon
 * ID:613040545-1
 * sec : 1
 * Date: 21 Apr 2019
 */
public class MobileDeviceFormV8 extends MobileDeviceFormV7 implements ActionListener, ItemListener{
    protected JMenuItem subCustom;
    public MobileDeviceFormV8(String string) {
        super(string);
    }
    protected void addSubMenu() {
        super.addSubMenu();
        //Add the menu item “Custom …” for menu “Color” 
        subCustom = new JMenuItem("Custom...");
        colorItem.add(subCustom);  
    } 
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run(){
                createAndShowGUI();
            }
        });
    }
    public void addListeners(){
        super.addMenu();
        //accelerators and mnemonic keys to menu File, Config and their menu items.
        newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        newItem.addActionListener(this);
        openItem.addActionListener(this);
        saveItem.addActionListener(this);
        exitItem.addActionListener(this);
        subCred.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
        subCgreen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
        subCblue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
        subCustom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
        subCred.addActionListener(this);
        subCgreen.addActionListener(this);
        subCblue.addActionListener(this);
        subCustom.addActionListener(this);
    }  
    public static void createAndShowGUI(){
        MobileDeviceFormV8 mobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
        mobileDeviceFormV8.addComponents();
        mobileDeviceFormV8.addListeners();
        mobileDeviceFormV8.setFrameFeatures();
        mobileDeviceFormV8.setVisible(true);  
    }  
    protected void setFrameFeatures() {
        super.setFrameFeatures();
        this.setTitle("Mobile Device Form V8"); //Set title.
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
